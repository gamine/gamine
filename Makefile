DESTDIR = 
PREFIX = /usr
BINDIR = $(PREFIX)/bin
DATADIR = $(PREFIX)/share
PKGDATADIR = $(DATADIR)/gamine
DOCDIR = $(DATADIR)/doc/gamine
SYSCONFDIR = /etc
DESKTOPDIR = $(DATADIR)/applications
ICONDIR = $(DATADIR)/icons/hicolor/scalable/apps
LOCALEDIR = $(DATADIR)/locale
MANDIR = $(DATADIR)/man/man6

CFLAGS = -Wall -g
CPPFLAGS = $(shell pkg-config --cflags gtk+-3.0 cairo glib-2.0 gstreamer-1.0)  -DDATADIR=\""$(PKGDATADIR)"\"  -DLOCALDIR=\""$(LOCALEDIR)"\" -DSYSCONFDIR=\""$(SYSCONFDIR)"\"
LDLIBS = $(shell pkg-config --libs gtk+-3.0 cairo glib-2.0 gstreamer-1.0)  -DDATADIR=\""$(PKGDATADIR)"\"  -DLOCALDIR=\""$(LOCALEDIR)"\" -DSYSCONFDIR=\""$(SYSCONFDIR)"\" -lm
LDFLAGS = -g 
CC = gcc
target = gamine
objs = gamine.o

$(target): $(objs)
	$(CC) -o $@ $^ $(LDFLAGS) $(LDLIBS)
	intltool-merge -d locale gamine.desktop.in gamine.desktop
	msgfmt -c -o locale/fr.mo locale/fr.po
	msgfmt -c -o locale/ru.mo locale/ru.po
	msgfmt -c -o locale/nl.mo locale/nl.po
	msgfmt -c -o locale/zh_CN.mo locale/zh_CN.po
	msgfmt -c -o locale/zh_TW.mo locale/zh_TW.po
	msgfmt -c -o locale/el.mo locale/el.po
	msgfmt -c -o locale/es.mo locale/es.po
	msgfmt -c -o locale/fa.mo locale/fa.po
	msgfmt -c -o locale/it.mo locale/it.po
	msgfmt -c -o locale/pl.mo locale/pl.po
	msgfmt -c -o locale/ro.mo locale/ro.po
	msgfmt -c -o locale/sv.mo locale/sv.po
	msgfmt -c -o locale/uk.mo locale/uk.po
	msgfmt -c -o locale/be.mo locale/be.po
	msgfmt -c -o locale/de.mo locale/de.po
	msgfmt -c -o locale/tr.mo locale/tr.po
	msgfmt -c -o locale/da.mo locale/da.po

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) $(CPPFLAGS)
clean:
	rm -f $(target) $(objs) *~ locale/*.mo locale/*~ *.desktop

install:
	mkdir -p $(DESTDIR)$(BINDIR)
	mkdir -p $(DESTDIR)$(PKGDATADIR)/sounds
	mkdir -p $(DESTDIR)$(DOCDIR)
	mkdir -p $(DESTDIR)$(ICONDIR)
	mkdir -p $(DESTDIR)$(DESKTOPDIR)
	mkdir -p $(DESTDIR)$(SYSCONFDIR)
	mkdir -p $(DESTDIR)$(MANDIR)
	mkdir -p $(DESTDIR)$(LOCALEDIR)/fr/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/ru/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/nl/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/zh_CN/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/zh_TW/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/el/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/es/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/fa/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/it/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/pl/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/ro/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/sv/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/uk/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/be/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/de/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/tr/LC_MESSAGES
	mkdir -p $(DESTDIR)$(LOCALEDIR)/da/LC_MESSAGES
	install -m 755 gamine $(DESTDIR)$(BINDIR)/
	install -m 644 pencil.png $(DESTDIR)$(PKGDATADIR)/
	install -m 644 gamine.png $(DESTDIR)$(PKGDATADIR)/
	install -m 644 sounds/* $(DESTDIR)$(PKGDATADIR)/sounds/
	install -m 644 AUTHORS README.pencil README.sounds README.locale README ChangeLog COPYING $(DESTDIR)$(DOCDIR)/
	install -m 644 gamine.conf $(DESTDIR)$(SYSCONFDIR)/
	install -m 644 locale/fr.mo $(DESTDIR)$(LOCALEDIR)/fr/LC_MESSAGES/gamine.mo
	install -m 644 locale/ru.mo $(DESTDIR)$(LOCALEDIR)/ru/LC_MESSAGES/gamine.mo
	install -m 644 locale/nl.mo $(DESTDIR)$(LOCALEDIR)/nl/LC_MESSAGES/gamine.mo
	install -m 644 locale/zh_CN.mo $(DESTDIR)$(LOCALEDIR)/zh_CN/LC_MESSAGES/gamine.mo
	install -m 644 locale/zh_TW.mo $(DESTDIR)$(LOCALEDIR)/zh_TW/LC_MESSAGES/gamine.mo
	install -m 644 locale/el.mo $(DESTDIR)$(LOCALEDIR)/el/LC_MESSAGES/gamine.mo
	install -m 644 locale/es.mo $(DESTDIR)$(LOCALEDIR)/es/LC_MESSAGES/gamine.mo
	install -m 644 locale/fa.mo $(DESTDIR)$(LOCALEDIR)/fa/LC_MESSAGES/gamine.mo
	install -m 644 locale/it.mo $(DESTDIR)$(LOCALEDIR)/it/LC_MESSAGES/gamine.mo
	install -m 644 locale/pl.mo $(DESTDIR)$(LOCALEDIR)/pl/LC_MESSAGES/gamine.mo
	install -m 644 locale/ro.mo $(DESTDIR)$(LOCALEDIR)/ro/LC_MESSAGES/gamine.mo
	install -m 644 locale/sv.mo $(DESTDIR)$(LOCALEDIR)/sv/LC_MESSAGES/gamine.mo
	install -m 644 locale/uk.mo $(DESTDIR)$(LOCALEDIR)/uk/LC_MESSAGES/gamine.mo
	install -m 644 locale/be.mo $(DESTDIR)$(LOCALEDIR)/be/LC_MESSAGES/gamine.mo
	install -m 644 locale/de.mo $(DESTDIR)$(LOCALEDIR)/de/LC_MESSAGES/gamine.mo
	install -m 644 locale/tr.mo $(DESTDIR)$(LOCALEDIR)/tr/LC_MESSAGES/gamine.mo
	install -m 644 locale/da.mo $(DESTDIR)$(LOCALEDIR)/da/LC_MESSAGES/gamine.mo
	install -m 644 gamine.desktop $(DESTDIR)$(DESKTOPDIR)/
	install -m 644 gamine.svg $(DESTDIR)$(ICONDIR)/
	install -m 644 gamine.6 $(DESTDIR)$(MANDIR)/

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/gamine
	rm -rf $(DESTDIR)$(PKGDATADIR)
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -f $(DESTDIR)$(DESKTOPDIR)/gamine.desktop
	rm -f $(DESTDIR)$(ICONDIR)/gamine.svg
	rm -f $(DESTDIR)$(MANDIR)/gamine*
	rm -f $(DESTDIR)$(SYSCONFDIR)/gamine.conf
	rm -f $(DESTDIR)$(LOCALEDIR)/*/LC_MESSAGES/gamine.mo
	rm -f $(DESTDIR)$(MANDIR)/gamine*
